var express = require('express');
var router = express.Router();

const tugasController = require('../controller/tugasController')
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/register',tugasController.register)
router.get('/karyawan',tugasController.tulisData)
router.post('/login',tugasController.login)


module.exports = router;
