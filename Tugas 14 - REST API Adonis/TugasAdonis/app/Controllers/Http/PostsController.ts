import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'

export default class PostsController {
    public async kirim({request,response}: HttpContextContract){
        try{
            const newPost = schema.create({
                nama: schema.string(),
                alamat: schema.string(),
                phone:schema.string([
                    rules.mobile()
                ])

            })   

        const post =     await request.validate({ schema: newPost })

            response.ok({
                message : post
            })
        }catch(error){
            response.badRequest(error.messages)
        }
    }
}
