 import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
 import { schema  } from '@ioc:Adonis/Core/Validator'

export default class BookingsController {
    public async data({request,response}  : HttpContextContract){
        try{
            const newBooking = schema.create({
                nama: schema.string(),
                namavenue: schema.string(),
                waktu :schema.string(),
                   

            })   

        const post =   await request.validate({ schema: newBooking })

            response.ok({
                message : post
            })
        }catch(error){
            response.badRequest(error.messages)
        }
    }
}
