// import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import { HttpContext } from "@adonisjs/core/build/standalone";

export default class TugasesController {
    public async index({response} : HttpContext){
        response.status(200).json({
            message: "Test route"
        });
    }
}
