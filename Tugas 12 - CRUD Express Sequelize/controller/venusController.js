//import mmodel

const {venues} = require('../models')

class venusController{
    
    //fungsi tambah data
    static async index(req,res){
    
        try{
        let id = req.body.id
        let name = req.body.name
        let addres = req.body.addres
        let phone = req.body.phone

        //memangggil
        const  newVenus = await venues.create({
            id : id,
            name : name,
            addres : addres,
            phone:phone

        })
        res.status(200).json({status:"success",  message : "Data Berhasil ditambahkan"})
    }catch (error){
        res.status(401).json({
            status:"failed",
            message:"gagal menambahkan data",
            msg: error.errors.map((e)=>e.message)

        })
    }
    
    }
}
module.exports = venusController;